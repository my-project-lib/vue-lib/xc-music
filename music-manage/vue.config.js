// const { defineConfig } = require('@vue/cli-service')

// module.exports = defineConfig({
//   transpileDependencies: true,
//   chainWebpack: config => {
//     config.plugin('define').tap(definitions => {
//         Object.assign(definitions[0]['process.env'], {
//           NODE_HOST: '"http://localhost:8888"',
//         });
//         return definitions;
//     });
//   }
// })

const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:3000',
        pathRewrite: { '^/api': '' },//路径改写
      },
      '/serve': {
        target: 'http://localhost:8888',
        pathRewrite: { '^/serve': '' },//路径改写
      },
    }
  },
  lintOnSave:false
})