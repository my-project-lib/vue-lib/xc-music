# xc-music
小程音乐是一个基于 vue3 + springboot2 +mysql 的音乐播放及好友社交项目
# 1. 项目简介
[简单介绍]
>1.1 项目背景
本项目旨在解决音乐平台单一性的问题，利用springboot2 + websock设计开发出一款社交音乐产品。  
1.2 项目目标
本项目的目标是实现社交，通过分享动态和好友聊天提供听众最优质的社交服务。  
>1.3 项目范围
本项目的范围面向大众，旨在音乐交友。

# 2. 技术架构
2.1 技术选型
本项目采用了vue+java作为核心技术，结合springboot实现了接口渲染和帖子显示。
系统架构：本项目的系统架构图如下：
[系统架构图]
本项目包括以下主要模块：
首页：用于展示歌曲
歌手页面：用于展示歌手
MV页面：用于展示歌手MV
动态页面:用于展示用户发布的动态
好友聊天：用于好友之间聊天通信


2.2 前端框架
本项目采用了vue3作为前端技术栈，并结合elementPlus实现了页面渲染。
2.3 后端接口
使用了Java和springboot。

# 3. 开发环境
为了开发本项目，请确保您的开发环境满足以下要求：
```json
"dependencies": {
    "axios": "^0.27.0",
    "core-js": "^3.8.3",
    "element-plus": "^2.0.4",
    "Express": "^3.0.1",
    "less": "^4.1.3",
    "less-loader": "^11.1.0",
    "vue": "^3.2.13",
    "vue-router": "^4.0.3",
    "vuex": "^4.0.0"
  },
```

3.1 开发工具
推荐使用以下开发工具：

Visual Studio Code：Visual Studio Code是一个轻量级但功能强大的源代码编辑器，可在您的桌面上运行，可用于Windows，macOS和Linux。它内置了对JavaScript，TypeScript和Node的支持.js并具有针对其他语言和运行时（如C++，C#，Java，Python，PHP，Go，.NET）的丰富扩展生态系统。  
IntelliJ IDEA ：IntelliJ IDEA是软件开发人员的首选IDE。设计和智能内置了效率和智能，从而实现了非常流畅的开发工作流程体验，从设计、实现、构建、部署、测试和调试到重构！
Sqlyog：一个强大的MySQL GUI工具，可帮助数据库开发人员自动比较和同步模式，计划备份和查询等等。SQLyog 还提供 MySQL 管理支持，帮助数据库管理员在物理、虚拟和云环境中轻松管理 MySQL 和 MariaDB。

# 4. 模块详述
4.1音乐首页：负责展示音乐相关内容。 

4.2.1 页面布局  

首页：使用 elementplus 布局，包括轮播图，歌单表单，歌手表单。
歌单：使用 elementplus 布局，包括歌单列表，歌单详情，歌单评论。  
MV ：使用 elementplus 布局，包括MV列表，MV详情，MV播放。
4.2.2 组件设计  

表单组件：使用 elementPlus 中的 ul 组件，实现列表。  

4.2.3 代码实现
```html
<template>
 <div class="yes">
   <!--轮播图-->
   <el-carousel class="swiper-container" type="card" height="20vw" :interval="4000">
    <el-carousel-item v-for="(item, index) in swiperList" :key="index">
      <img :src="item.picImg" />
    </el-carousel-item>
  </el-carousel>
  <!--热门歌单-->
  <play-list  class="play-list-container"  title="歌单" path="song-sheet-detail" :playList="songList"></play-list>
  <!--热门歌手-->
  <play-list class="play-list-container" title="歌手" path="singer-detail" :playList="singerList"></play-list>
 </div>
</template>
```
```js
import { ref, onMounted } from "vue";
import PlayList from "@/components/PlayList.vue";
import { swiperList, NavName } from "@/enums";
import { HttpManager } from "@/api";
import mixin from "@/mixins/mixin";

const songList = ref([]); // 歌单列表
const singerList = ref([]); // 歌手列表
const { changeIndex } = mixin();
try {
  HttpManager.getSongList().then((res) => {
    songList.value = (res as ResponseBody).result;
    // 
    console.log(songList);
    
    
  });

  HttpManager.getAllSinger().then((res) => {
    singerList.value = (res as ResponseBody).artists;
  });
  
  HttpManager.getjournalings("/serve/journaling/all");
  onMounted(() => {
    changeIndex(NavName.Home);
  });
} catch (error) {
  console.error(error);
}
```
评论和点赞
```html
<template>
  <div class="comment">
    <h2 class="comment-title">
      <span>评论</span>
      <span class="comment-desc"
        >共 {{ songcommenton.length || songsong.length }} 条评论</span
      >
    </h2>
    <el-input
      class="comment-input"
      type="textarea"
      placeholder="期待您的精彩评论..."
      :rows="2"
      v-model="textarea"
    />
    <el-button class="sub-btn" v-if="songcommenton.length != 0" type="primary" @click="submitComment()"
      >发表评论</el-button
    >
    <el-button class="sub-btn"  v-if="songsong.length != 0" type="primary" @click="submitComments()"
      >发表评论</el-button
    >
  </div>
  <ul class="popular" v-if="songcommenton.length != 0">
    <li v-for="(item, index) in songcommenton" :key="index">
      <el-image class="popular-img" fit="contain" :src="item.user.avatarUrl" />
      <div class="popular-msg">
        <ul>
          <li class="name">{{ item.user.nickname }}</li>
          <li class="time">{{ formatDate(item.timeStr) }}</li>
          <li class="content">{{ item.content }}</li>
        </ul>
      </div>
      <div
        ref="up"
        class="comment-ctr"
        @click="setSupport(item.id, item.up, index)"
      >
        <div><yin-icon :icon="iconList.Support"></yin-icon> {{ item.up }}</div>
        <el-icon
          v-if="item.userId === userId"
          @click="deleteComment(item.id, index)"
          ><delete
        /></el-icon>
      </div>
    </li>
  </ul>
  <ul class="popular" v-else-if="songsong.length > 0">
    <li v-for="(item, index) in songsong" :key="index">
      <el-image class="popular-img" fit="contain" :src="item.user.avatarUrl" />
      <div class="popular-msg">
        <ul>
          <li class="name">{{ item.user.nickname }}</li>
          <li class="time">{{ formatDate(item.timeStr) }}</li>
          <li class="content">{{ item.content }}</li>
        </ul>
      </div>
      <div
        ref="up"
        class="comment-ctr"
        @click="setSupport(item.id, item.up, index)"
      >
        <div><yin-icon :icon="iconList.Support"></yin-icon> {{ item.up }}</div>
        <el-icon
          v-if="item.userId === userId"
          @click="deleteComment(item.id, index)"
          ><delete
        /></el-icon>
      </div>
    </li>
  </ul>
```
```js
setup(props) {
    const { proxy } = getCurrentInstance();
    const store = useStore();
    const { checkStatus } = mixin();
    const { playId, type } = toRefs(props);
    // 歌单或则歌曲id
    const songid = ref(props.playId);
    //歌单的评论
    const songcommenton = ref([]);
    //歌曲的评论
    const songsong = ref([]);

    const commentList = ref([]); // 存放评论内容
    const textarea = ref(""); // 存放输入内容

    const iconList = reactive({
      Support: Icon.Support,
    });
    const userId = computed(() => store.getters.userId);
    const songId = computed(() => store.getters.songId);

    // 获取歌单和歌曲评论
    async function getsonyes() {
      await HttpManager.getcommenton(songid.value).then((res) => {
        songcommenton.value = (res as ResponseBody).comments;
      });
      await HttpManager.getsongcommenton(songid.value).then((res) => {
        songsong.value = (res as ResponseBody).comments;
      });
    }

    getsonyes();

// 添加歌单评论
    async function submitComment() {
      if (!checkStatus()) return;
      let nowtime = new Date();
      var ok = {
        content: textarea.value,
        timeStr: nowtime.toLocaleString(),
        user: {
          nickname: proxy.$store.state.user.username,
          avatarUrl:
            "http://localhost:8888/uimg/user.jpg",
        },
      };
      songcommenton.value.unshift(ok);
    }
    // 添加歌曲评论
    async function submitComments() {
      if (!checkStatus()) return;
      let nowtime = new Date();
      var ok = {
        content: textarea.value,
        timeStr: nowtime.toLocaleString(),
        user: {
          nickname: proxy.$store.state.user.username,
          avatarUrl:
            "http://localhost:8888/uimg/user.jpg",
        },
      };
      songsong.value.unshift(ok);
    }

    // 删除评论
    async function deleteComment(id, index) {
      const result = (await HttpManager.deleteComment(id)) as ResponseBody;
      (proxy as any).$message({
        message: result.message,
        type: result.type,
      });

      if (result.success) commentList.value.splice(index, 1);
    }

    // 点赞
    async function setSupport(id, up, index) {
      if (!checkStatus()) return;

      const params = new URLSearchParams();
      params.append("id", id);
      params.append("up", up + 1);

      const result = (await HttpManager.setSupport(params)) as ResponseBody;
      // if (result.success) {
      //   // proxy.$refs.up[index].children[0].style.color = "#2797dd";
      //   await getComment();
      // }
    }
```
歌曲
```html
<template>
  <el-container>
    <el-aside class="album-slide">
      <el-image class="album-img" fit="contain" :src="srcimg" />
      <h3 class="album-info">{{ classification }}</h3>
    </el-aside>
    <el-main class="album-main">
      <h1>简介</h1>
      <p>{{ introduction }}</p>
      <!--评分-->
      <div class="album-score">
        <div>
          <h3>评分</h3>
          <el-rate
            v-model="value"
            size="large"
            :max=10
            disabled
            show-score
            text-color="#ff9900"
            score-template="{value}分"
          />
        </div>
        <div>
          <h3>我的评分      {{ score }}</h3>
          <el-rate
            allow-half
            :max=10
            v-model="score"
            :disabled="disabledRank"
            @click="pushValue()"
          ></el-rate>
        </div>
      </div>
      <!--歌曲-->
      <song-list class="album-body" :songList="songLists"></song-list>
      <comment :playId="songListId" :type="1"></comment>
    </el-main>
  </el-container>
</template>
```
```js
 setup() {
    const { proxy } = getCurrentInstance();
    const store = useStore();
    const { checkStatus } = mixin();
    const value = ref(3.7)
    
    const currentSongList = ref([]); // 存放的音乐
    const songListId = ref(""); // 歌单 ID
    const score = ref(0);
    const rank = ref(0);
    // Imgsrc
    const disabledRank = ref(false);
    const songDetails = computed(() => store.getters.songDetails); // 单个歌单信息
    const userId = computed(() => store.getters.userId);

    songListId.value = songDetails.value.id; // 给歌单ID赋值
    // 随机评分
    function setpingfen(){
      let randomnumber = Math.round((Math.random()*4+7)*10)/10
      value.value = randomnumber
    }
    setpingfen()
    
    // 提交评分
    async function pushValue() {
      if (disabledRank.value || !checkStatus()) return;

      const params = new URLSearchParams();
      params.append("songListId", songListId.value);
      params.append("consumerId", userId.value);
      params.append("score", (score.value * 2).toString());
      alert("你好")
      try {
        const result = (await HttpManager.setRank(params)) as ResponseBody;
        (proxy as any).$message({
          message: result.message,
          type: result.type,
        });

        if (result.success) {
          disabledRank.value = true;
        }
      } catch (error) {
        console.error(error);
      }
    }
```
4.2登录页

4.2.1 页面布局  

用户注册页：使用 elementplus 布局，包括输入用户名、密码、邮箱的表单。
用户登录页：使用 elementplus 布局，包括输入用户名、密码的表单。  

4.2.2 组件设计  

表单组件：使用 elementPlus 中的 Form 组件，实现表单校验。  

4.2.3 代码实现

```html
            <el-form
				ref="signInForm"
				status-icon
				:model="registerForm"
				:rules="SignInRules"
			>
				<el-form-item prop="username">
					<el-input
						placeholder="用户名"
						v-model="registerForm.username"
					></el-input>
				</el-form-item>
				<el-form-item prop="password">
					<el-input
						type="password"
						placeholder="密码"
						v-model="registerForm.password"
						@keyup.enter="handleLoginIn"
					></el-input>
				</el-form-item>
				<el-form-item class="sign-btn">
					<el-button @click="handleSignUp">注册</el-button>
					<el-button type="primary" @click="handleLoginIn">登录</el-button>
				</el-form-item>
			</el-form>
```
```js
    export default defineComponent({
	components: {
		YinLoginLogo,
	},
	setup() {
		const { proxy } = getCurrentInstance();
		const { routerManager, changeIndex } = mixin();

		// 登录用户名密码
		const registerForm = reactive({
			username: "",
			password: "",
		});

		async function handleLoginIn() {
			let canRun = true;
			(proxy.$refs["signInForm"] as any).validate((valid) => {
				if (!valid) return (canRun = false);
			});
			if (!canRun) return;

			const params = new URLSearchParams();
			params.append("username", registerForm.username);
			params.append("password", registerForm.password);

			try {
				const result = (await HttpManager.signIn(params)) as ResponseBody;
				(proxy as any).$message({
					message: result.message,
					type: result.type,
				});
				console.log(result.data[0]);

				await HttpManager.getState(1, result.data[0].id);
				if (result.success) {
					proxy.$store.commit("setUserId", result.data[0].id);
					proxy.$store.commit("setUsername", result.data[0].username);
					proxy.$store.commit("setUserPic", result.data[0].avator);
					proxy.$store.commit("setToken", true);
					changeIndex(NavName.Home);
					routerManager(RouterName.Home, { path: RouterName.Home });
				}
			} catch (error) {
				console.error(error);
			}
			// console.log(proxy.$store.state);
		}

		function handleSignUp() {
			changeIndex("注册");
			routerManager(RouterName.SignUp, { path: RouterName.SignUp });
		}

		return {
			registerForm,
			SignInRules,
			handleLoginIn,
			handleSignUp,
		};
	},
});
```
# 5 后台管理系统
5.1 ：项目模块  
1.登录页面：后台系统的登录  
2.首页模块:主要有四个卡片的一个echar折线图组成，卡片实时展示用户数量以及在线人数，折线图显示了用户随时间的活跃度变化。  
3.用户管理页面:主要是对用户进行一些增删查改的操作  
4.用户分布:主要是展示了所有用户的性别以及地区分布  

5.2 折线图及card数据代码
```js

const { proxy } = getCurrentInstance();

//用户人数
const userCount = ref(0);
//在线人数
const songCount = ref(0);
// 日访问量
const DailyVisits = ref(0);
// 积累访问量
const AccumulateVisits = ref(0);
const Visits = {
	xAxis: {
		type: "category",
		data: ["前五天", "前四天", "前三天", "前二天", "前天", "昨天", "今天"],
	},
	yAxis: {
		type: "value",
	},
	tooltip: {
		//提示框组件
		trigger: "item", //item数据项图形触发，主要在散点图，饼图等无类目轴的图表中使用。
		axisPointer: {
			// 坐标轴指示器，坐标轴触发有效
			type: "shadow", // 默认为直线，可选为：'line' | 'shadow'
		},
		formatter: "访问量 <br/>{b} : {c} <br/>", //{a}（系列名称），{b}（数据项名称），{c}（数值）, {d}（百分比）
	},
	series: [
		{
			data: [],
			type: "line",
		},
	],
};
const { changeSex, routerManager } = mixin();
function getOnlineNumber(){

	const breadcrumbList = reactive(
        {
          path: RouterName.Consumer,
          name: "在线人数",
        },
      );
      // console.log(id); 
      proxy.$store.commit("setBreadcrumbList", breadcrumbList);
	routerManager(RouterName.OnlineNumber,{path: RouterName.OnlineNumber});
};
// 获取积累人数
setInterval(function () {
	HttpManager.getjournaling("/journaling/data").then((res) => {
		// console.log(res);
		DailyVisits.value = res.online;
		AccumulateVisits.value = res.accumulate;
		Visits.series[0].data = res.visitList;
		Visits.series[0].data.push(res.online);
		const VisitsChart = echarts.init(document.getElementById("Visits"));
		VisitsChart.setOption(Visits);
		//获取用户总人数
		HttpManager.getAllUser().then((res) => {
			userCount.value = res.data.length;
		});
		//获取在线人数
		HttpManager.getOnline().then((res) => {
			songCount.value = res.length;
		});
	});
}, 1500);
```

# 6. 测试与调试
>6.1 测试环境  

操作系统：[环境要求，如 Windows 10、macOS 11 等]
浏览器：[环境要求，如 Google Chrome、Mozilla Firefox 等]
其他软件：[环境要求，如 Node.js、npm 等]
>6.2 测试方法  

使用 [测试工具，如 Jest、Mocha等] 进行单元测试.  
>6.3 测试计划  

用户注册：测试用户注册的 API 接口是否正常。
用户登录：测试用户登录的 API 接口是否正常。

>6.4 调试工具  

使用 [Chrome DevTools、VSCode Debugger 等] 进行调试。
>6.6 调试方法  

在 DevTools 中断点调试。
# 7. 附录
7.1 示例代码
下面是实现搜索功能的 好友通信 组件示例代码：
```js
const form = reactive({
      desc: "",
    });
    // 切换对象
    const changingover = ref("聊天");
    // 搜索内容
    const SearchContent = ref([]);

    const { proxy } = getCurrentInstance();
    //上方的显示
    const fanusername = ref();
    // 好友列表数据
    const firendList = ref([]);
    // 用来装总信息的
    const firendsorr = ref([]);
    // 搜索框里面的值
    const input1 = ref("");
    //搜索到的好友信息
    const Searchinformationyes = ref();
    //好友添加消息的出现
    const FriendAddMessage = ref([]);

    const userId = ref(proxy.$store.state.user.userId);

    const pyidyes = ref();
    //用来装好友列表的
    async function getfirendsall() {
      console.log(userId.value);
      firendList.value = await HttpManager.getfiendsall(userId.value);
      // console.log(firendList.value);
    }
    getfirendsall();
    //点击切换好友的函数
    function SwitchFriends(item) {
      fanusername.value = item.pyname;
      pyidyes.value = item.prid;
      //获取好友聊天内容
      async function getfirendsconst() {
        firendsorr.value = await HttpManager.getCatserve(
          userId.value,
          pyidyes.value
        );
      }
      getfirendsconst();
      setInterval(function () {
        getfirendsconst();
      }, 6000);
    }

    // 好友消息显示
    async function Message() {
      let yes = await HttpManager.HaoYouXiaoXi(userId.value);
      // FriendAddMessage.value = redst
      console.log("yess", yes);
      console.log(yes[0].pyid);
      let good;
      good = await HttpManager.getUserOfId(yes[0].pyid);
      FriendAddMessage.value = good.data;
      console.log(FriendAddMessage.value);
    }
    async function agree(value) {
      location.reload();
      let type = 2;
      await HttpManager.Confirmconsen(userId.value, value.id, type);
      console.log("yes");
    }
```
复制代码
7.2 资源链接
下面是本项目中用到的一些资源链接：

[Vue 中文文档](https://cn.vuejs.org/)。  
[Vue Router 官方文档](https://router.vuejs.org/zh/)  
[Axios 中文文档](https://www.axios-http.cn/)  

7.3 开发文档
下面是项目开发中需要用到的一些文档：

前后端分离架构设计
开发流程与规范
代码规范指南

